import { Component } from '@angular/core';
import {  NavController, NavParams, LoadingController, Loading } from 'ionic-angular';

import { AngularFireDatabase } from 'angularfire2/database';

import { Network } from '@ionic-native/network'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  //connectedRef: any

  refDB: any

  load: Loading

  public uid: string;

  connectSubscription: any;

  public image = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhUUExMVFhMXGBgbFhgXGRoZGBkeFhYYHiAYIBoYHSggHBolHRcgITEiJSkrLjAuGh8zODMtNygtLisBCgoKDg0OGxAQGy0lICUwLS0tLS0tLS0tLy01LS0uLS0vLS0tLS0tLS0tLS8tLS0tLS8tLS0tLS0tLS0tLS0tLf/AABEIAQ4AuwMBEQACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAABgcDBAUCCAH/xABFEAACAQIDBQUEBwUHAwUBAAABAgMAEQQSIQUGMUFhEyJRcYEHMpGhFCNCUmKxwXKCkqLhFTNTg9Hw8UOys1Rjc8LSJP/EABoBAQADAQEBAAAAAAAAAAAAAAACAwQBBQb/xAAzEQACAQIFAAgGAgIDAQAAAAAAAQIDEQQSITFBEyIyUWFxgfAFkaGxwdEU4SNCM1LxU//aAAwDAQACEQMRAD8AvGgFAKAUAoBQCgILvv7QlwjGGBFlmX3yxISPTQG2rN+EEWHPlUHK2hopUHJXeiK8k3yxco7X6U6yMrFUjeyhtQFEdzfUWF78ajeVyxQgtLHR3b9omNifLOTKPtRyKEkA8VOUH43HlxpnaLP49Oa6ujLh2VtGPERJLEbo4uPEeIPUHSrE7mGUXF2Zt10iKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAc3eTaJw+ExE4ALRRO6g8CVUkA9L1x7EoRzSSKb3L2SMVPnmUyorMZCSNZCucu44sCxtYcySayVJZV4nozlrlWyLTwuycOrZ1ghD6d/s1zacO9a/Ks2eT0uUyRx97NlQYkrExKzKjyIQvhZbl8ulmZTYMD0IvUoScdeDq3OZ7Fdql1nhbQ3WS3g3uSADkAUU/v16ENNCvE62kWdUzKKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFARr2kSAbMxV+Bjy/xsF/WuPYspdtFUezvGwfRcV9JZRF2n1mYm32TbTXieFZayeZZTZHVO5bGwtqw4hBJDIsiHS6nmOII4g9D0rNKLi7M5PVaET3i3/hhxYwwjaTvKsjggBC9rAD7RGYE/rVkKDccx1SS0ZE9xcd9D2uysRlaaSJv81gV/myfCtcHoiE1mg13F+VaYhQCgFAKAUAoBQCgFAKAUAoBQCgFAKAUAoCuvbZtMJhIoB708ouPwRd4n+PJ8a5Ivw661yrd1YExOHxWELBXLq6tbgwYkE+IOXL69KoqPLJSNEVmTiTbcDZX0AlZZkM+IuVRCSuWEakEgXbvi+g5DW1UV559loiUI20Zi2jsHCNivpQMqv24vG4yxyPGblxmGbL3bgg2Jt469VWSjlZ1UnJ3XBAdtSFsVibce14jQ+4vMc60Q0ijkVqy+/ZxvKcdg1d/76M9nL1ZQDm/eDA+dxyq1MxVYZZaEprpWKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgKM9suOz7QC37mHhW/RpCWb+XJUJb2NmHVouRX+6mMKYp7GxdfmP8AmlWF43FGoulaLY2FIyxoqkNqS5JN9QxuNDc5raG2hJ4157RuqScpXP3bcoKOJMpF7rYEFRYcTc3N7m4tobW8exjcjGcou6ZVWHxYeeQ8QzGx8baVtatFFFKV5Mtv2RbWhgMmEbuyTSdpGx4N3FUp0IyXF+Ob4qcuCvFUn21sWpVpiFAKAUAoBQCgFAKAUAoBQCgFAKAUAoDhby72YXBIxllTtQpKRZh2jkDRQo11OlzprRkoxcnY+c9uY55laV2zSSMGc+JbU+Q5AcgAKrjvqehOyhZEZxilXupsRwNakrwPLneFVkp2HvoFAWcNdSCGW/EeIH5aisU6L4PQhXW0jBvNvV2wZYVyhrdo9rFrCw6nQW1qVOlbchVrX0iaGCTK0Y8/yqcnoTgrNI6mLxzowdDZ0K5Dp3WVswbXS4Kg+lcpwzSSRKvUyQk33WLe3P8AaPI8ZO0I1jAF1lXuhvG6MdPMG3QV2tVpwlli7+Wpio0qs45pK3mSLC+0HZzgn6VGtjbvHjpxFr3FISzK9mvMSg0zMd+tnf8ArIfj/SpEcrPyPfrZ7apiA9v8NJH/AOxTXJSUe07HVCT2QO++D5PI3lFID8GUGqniKa5LVh6j4OftH2iYeNSVVjbm5WNfjqR8Kg8Stopssjg5/wCzsc3d3ezFbRxGWDuQoQ0jqvcAv7pLglma1gFtpc30sUZVZvuRKpSpUo66ssWtJiFAKAUAoBQCgPxhpppQFcYvbm1MO2SXJa+krx3Q+HeQqvobHpWKVWrTXWVz0I4elV1g/QjO8m0NpuxT6eGU8ol7EeV0uxHmxrtPGxktYtfU7/CktiCTYJlmySAKQrNe4s19Lg8/ev6VfKqpQvE5TpNTs+DRxbWjy8SSPQXrsVrdio7RsYZMKHLE+nSrVsZ5pZmz2uFW1mS/WpDRmDaCqFARLdaIhJpK5mnjIAI94WI9KkqdqbciE8ReslDWx39kRoyyYiUfVRKLae9I3uoOV7BmPgB1FYqmZdWO7+x6DcZWv5mi0s2LkGhb7qD3VHj4epqyMYUY3ZS5SquyJDhNzJiuaVlij8SQo/iewv0F6zyxq2grlkcN/wBmbqNsnBn6xxM45Bc2v74J9Qtqpf8AJrbaL5E3CnDf66v5I/Nq+1aPJ2eHwndHAuQB52FwfUCkPhr3k/yQ6aK21+n9kOx+9uLn0zhF+6gsPne3patkcLSp6v6nFWqS0j9CWbl+zPFYxllxJeKHjnfWRh+BW1/ebTgQGqWr0WiIyqRhq3ml9EXnsXY8OFiEMCZUGviSTxYk6knx8uQqaSSsjJObm7s366RFAKAUAoBQCgFACKA5eP3cwk2smHjJ+8FCv/EtmHxqLhF7otjXqR2kygt/4LNIiroJXSPmbCXQX43soqNKioVL8WbNdeq50Vp1ro5ceALQPI1hkdQxOlrgc/Cqpz6yS7ieXS7OzhsHHZGkQO7aRqFzM1gCdLgEAEXLeIsDrVOabuouyW7OVFCFrq7fCP3F4GOeNnjTs2S97La2XiCoNvXQ13palKeWTuI04VYZoqxr7F3ajnlVXchBq7NYAAcgBqSeQFzUp4ucdVuc/jRas9Te2xtLBYTPFh8MZpQLNJKSEBtyW+Y2HiR5VbClXxNpzloZJVKWGbhFfLX5tm5hN05cQsOEByxwjPMdB35ALgm2rDLbW9goGgrPPFZG5c8G1wja70Nz+0sPhR2OBRZpRo0xF4lPiv8Ait193q3CuU8NVxDz1NEJ1Y01ZaeHPr3fcje9GzcU8TzvK7sBcksb2GpA8Ba+g00r0I4enTWiMjxEpOy0XciI4LYeIlF0ibKOLHuqPNmsKSqwjuycaU5bIku7vs6xOKI7MBk5yXKwj/MI7/7gbzFQVVy7K+ZKUIU+2/RFy7oezbCYKzsBNONc7DuqfwJqB5m56jhXVHW71KZ121aOiJrUigUAoBQCgFAKAUAoBQCgI+u9sJxz4KzZ0iMhfTKcuW6DW9wHB8OPhUVJNtdxY6bUU+8qX2ijsyjAd4NmPXMCD5catVPpFlLqlbokp9z/AK/J+4WBJNkTAclVhfjZTax63NeS80MVr3muaVSgsuzX4/o0tlYuHECMmVo5U1Uq4R1JtcakcxoRU5Z6LatdPvWh1whXSlezOthZQVbBYMpJiJ+67AhhEn23dhpmI0ABNtb2Nr0tOUulq7L0u+EvATtTiqdPjf37u/UlGP2emGSKDDxvPKSucrbORcZ5CxsF0uBwHIcKzw/yScpNJe9AqrUbtEO2vsGGHaMaK6PE0iuwBDFVU5nVhy0B417NDEyeFk3ulb8I8yrQvXi1pme3vhmltHaMzRtmkyxYh3eRE997Nl77ck7vujjrfpzD4RNqcjbVrqLyx3Rt7vKAQcgy/OvRktLGNonUWHSSMkW8v6Vnl3Mq1TudTZe4eDGWSTtMS1gVM7ZlF9dIlAjA8O7pVEaUY7I0zxNSXNvIlgFtBwqwzn7QCgFAKAUAoBQCgFAKAUBixeIEaM7cFBJ9BRgqiTYLttWCZX5StKRzzKVZfI5x8K8/BVnPNfzPSrwSS8DR9pSLnOfQF4/hmW/wF69SDaTcd7P7GKooyjlls2vuaEZthsQlgo7JgApuLB1I16qL+teM9aid7+9T12rQta1uPnYh8WBQKGkPc093Un416Lqtu0Vr4mHokleT0J7ubFFhkfERxlEdQe8e9lHDXUDMdfDUV5mKlKpJQbvbuNlKnFQvbc5u8+IxiYiVZ3ZASCFUkKVtpqPeFvHrXqYGjQlTTSu/E8XHYirGVk7LixGocRlkBGmjD4qRWvGR/wAL9Cj4bK+ITfiY0nyBQdSzsPmW/X51HD/8aPQqr/I7ku3dxKq6kju3FxwuKskrojJFiRwAPmWxRuGmlj/Ss19NTOyUbIb6sD7pI/UfIiuHUbtAKAUAoDzLIFBZiAoBJJ4ADiaHUm3ZFY7lSPPtEz65m7R3J5qRZU8gSoA/CPCsFFuVXN5n0OPhClhFBeC9eWWXLiEUqGZVLGygkAsfAX4mtzaR88ot7Iy104KAUAoBQFUe1Ha2IxEyYXCq5hj7R5pFNg0ixvli0OoVveBv3sv3TV1KGtzzsbi6MYODlZ3SfzV/oc7c/bkpdO0UiUd2RHupzdbi6g6G9eTVp/xaum34/o+lo1oYygpxaflrqcbfjaBxeIVEGpJzIdChXQqw5EG9/LS969SnVpxp9JfQ8vEUpzkqMd3r6d/kbCYDsdmTSZsw7MqG5G8gtbpZSB0ryHPpMRtbU9ZLo6Ki3ey39CN7r4YzOuGtcS2H7NiCzfwBvW1bcS8kek5RmpO/VexPd7NoQ4cJh5IjIHBLRg5VIUgKrMNQOJsOPdrBg6FSq3ODtbkuxWIpwjaXPt+/Mi2823nxXZ50RFjXKire4HgWJueFe1hcL0N3mbueBjMUqiyxjzfxIriW8OVaquXI8xXhc3SJx39/Q5805LX8DpVGHoOF2/kerWxCqWyol+w8XmUMPUdam1ZlieZXLL3T2oMpjbgSMpPLmR8aonHkpmiYbPlySAHg4Hx1t+vyqorRA9/d95O2eGGXsooyQ8gJBZhxGYahQdNBckHjpVEpSqTyRNeWNGk6s1fw+3hr46ER2Bv3i1nUriJniDd4PkyFb62VlZh5579BWtU6dJa3fqeVVniqi6uWMnskr6+L09dPUkWP9qeKF7CFVHMo17de/a58BWN1pSfVX7PXpYTLBdK9ebaL66nnZHtns1sTGrJfVoxlYDxykkN5aVYnUXaRyVGm+zI7O+e9q4iHLhZFaE5S7Am7gqGyjTQC/eB1uLG2oOfEVr9RG/A4NxSrS9P3+vmc/czeqONXWMK0xYZs1wFjVdNQNSWJHTieQNcKnRRvbV/Y0V8M8VO17JL6nU2/taKbELJBKshMWVlU3MZVjcHwJzcOPd8q5imm1JMl8NpNU5U5xtZ79/H0/J3MXvlFFh1OdJMSyLaNDmu5H2ilwi31N/DS5rX06jBN72PLhgJVKzjFdW+/h+TQwOJ2rKiuHjs3hGoHG2l2vaqY1a0ldI11cNgaUnCUndE5raeIKA1dqJI0MgiIEpU5CdBe2mvLzqMk3FpEoNKSvsUrjcEjPM2UrII5lytoczIVK9Dqf98asHWt/jnutv0V/FMJOFqtLWDlFytxre/lpr3b+UVwW2ZoGXMxaNTwbUqB4E6gdOFaq9NVoZXvwMIoYes6sFbN2rc+Nu9d++6M239tdpiziUAuwQ6fasoU36kDU9aqw1FSwzpvi5diJSo4qNSO345R1t6toKMDHCNA0igqPCMA2H8R+FefhoN1XI9TEJRjZ7e/0dT2ORxvjXZUy9nCbE63Lso9LAH41Zi1JRSbvf3+THJpxdlbb39Dq7x4jDscX9KePvSH6OEBMyNGMlzyyHLwPX0pw6qKSdFO637iVZR6NKo9Le/G5WmJvzr6SOx8zPtaHOkF7+Y/WoS1qRT8fx+zZR0pS9F6a/o1sTBbUVokiyMjc3cxWWTLybh5j+n5VTNcmqjLXKWru0gYixF/Cs82Tma+8205oXsJrFCGsDcZlIK6HTSwqmUurdClDM0iudtTllFySWck9ba/maowi6zZqxrtBLxN3Y8YC+dyfJQT+h+NdxUtolGAp3nKo+NF93+CP7WxZZrchqfM12jCyuXYid3lMOFw5k0AJ8fX9asnUUEQo0ZVXZFmbqwsIpFdRqNBwsLcdeP/ABXj1XrdH0UHlhaeh63X2MYu8wJJAXQHRjxB8NfiLHnSpPM7HKFqad3qTTYW5L5GzN2QcknS7m/E20y8f6VdTw0p6y0MuI+J0qfVgs32/s7eyNycPD731nhcWHqBxrRDCxTu9TDW+K1ZK0Or9yTIoAAAAA0AHAW5VqSseY227s/aHBQHA313pi2dhmmfvOdIowbGR7aDoo4k8h4mwI6lc+ccHvtO2KkmxDZzI2ZvBdALAfdAAAHgOdU1qCmrrc24bEZOpLb7Ew3l2bDiMP8ASsJYqR9ag4xt425Kfl5EVHD4h36Opv8AczV8K6LzQ7P29/8AnhD8FsCWWESREuVLK0fMa8QOYtbrVzxMaVVqWly3+O6tFSXj6HnamKZgqlWDKSSDoRe3KoUaTi78GivWU1bk6W5m35MLM5Q5S6Aa6cDe1QxlLpEvA5hnF3jI2WhmxWKlaJMzOc7Ee6ubr53041fQr06FBZ3tp4sxYvC1KlZqntZeSOhtfcnGRQdsTEw+6CQ/oGAB+NKfxSjOVmmjLL4bNPRpshuHkLXuCNfyr0KVpvN3EJRdKOXv39DKy3FaCtM5pujAjiDcelVNF8ZW1RYWwNsLkDj3rHS/A6XFZprg3RtNXOdtLFlgxJ1P+tUVlaDNNJdZHHx8H1cLE2Ddr48Uy93Tmbj41VhP9vT8lPxBtZbLv/Bt7Ok7kp8IW+bRj9TVOI/5F77y7BpKj6sjTcT5mtcOyimfaZ2t2J1V2DlQrWILGwuNLXPDl8DWfE020mjf8PrxpuUZ86rzJtuvtxMRjYcJGCQ5YM62soVGbS41Pd8tayxwjestDRX+JqzUFe3yLmwGyYYdUQZvvnVvieA6DStkKUIbI8WtiqtXtPTu4N6rDOKAUAoDT2vtOLDQyTzNljjW7H8gBzJNgBzJAoD5p3r3kl2hiGnkuq+7FHfSNL8OrHix5noABNItirGvufubJtHFdlHdY1s00nJFJ+btYhR0J4A1xs5LQu/bns3hMCrgbYaaNMqsL5ZAL92W2rE3Pf1IuePCqZ01Pcso4iUNOCrNjQSYaR4ZBkmRiJIydQTqCCOIIIIIuLEVjxEXfrHqYeUXHqnbxDQy27YA/wDyIxH8Sa1ng5w7L+RZOEZbo0ts7u4LLmhlCt91ZVccPuuAw+daaeLqp2kr+hkeGizrYvdB4NnK0c7jtNTkABOYcGI1y6WtpwAqp1XOopySt+jkbK9KPBxPZzneSWI+4MqyLpazZxcjhmUrmB6Grcaoxytc/wBEqM5TjLNvHn8HMR8OWcSq9y2YSIRdbqNCh0Ivc8jrXrfD3/jfmeB8ZhiOmjKk1a3Zfm9brVHo7BL64eVJvw+5J/A3H0vW+55P83JpXg4+O6+a/Njg7SwLISGUqw4ggg/A1GR6NGrGavF3Rp4bEvGbobeI5HzFVNJmqE3F3R3NmFsSQqITI1wFGpJCltPRSbelZarjfI+T0qVS8c5gWa8RidTmilz5TcNYjI668CCqcuR8Ky4d2nryrHcdFuldPb3/AGZtnOCxA0V1ZNTwzA218yD+7TFRtaRHAS0cHvv+yP4yEhiDcH9RxFWUpXjY5Xi1K/eYYoyPtH4mrSgs32E7LMmOaa3dhjbX8UndA9Vz/Cq5b2JPSD8dPf0L+oUCgFAKAUBUPttnmdo4wScKgzSBRwkudXP3QtrcgSb62tBVoZ8l9TRGhPJ0ltCtdg7AmxuIXD4canVmPuoo4u1uQvw5kgc6ubIN2R9I7r7vQ4HDrBCNBqzH3pGIF3Y8ybegAA0AqBUbu1McsEMkz+5GjO3koJ+OldSuG7HzJZsTiDJKSZJXZ5CCeepA8ANAByAHhVmIap0m/kRwOerXSvpz5GfbvaYVo1SWW7KGILG4B4cLcqw0FGsm5RR61aUqbSjJ+poybcxL6PLMw8DI5HwvV6w9NbRRndeb5JVsnH4maDIZMsSgEot7Wz2LMb3Omutx0FefVhGE9DfB54qbMcu1jho5MNHD2cxLieRtGObQqFGgFha5PAmwW5NaKOEdZqpJ6cHn4jHQo3ild/S/e3+iM4RLFgdSdbnia9mklFZUeLOtKo80jZyVaRub42tKUySATJ92QZreTe8PQ0exklg6ebNDqvvjp81s/kcSbZ6uSVDRdJPd9GsD8RVVzRHEShpJqXlv8jLsKdsLOkgIJRlkGU3BMbBrX4agW9axYpbSPWwFVVVKH3LJ9qO5EjSDaOBBYkB5ET3r20lQDjcWuBrz1uaolF3ujVSqRlB0qm23hbuZWqbYjl/vowrf4sICm/i0eisb8xlNalKNaNnueb/FrYKeajK8eIyu15KW69bnnHZJLm4Y87aXtwkAOuvMVgalSlY9qMo14Zrea5TObhsA8jrGgLsxsqqLsx8APGr1WvwUuhbd6H0t7Od1v7Pwgjaxmc55bcASAAgPMKBa/M3POpJPky1ZqT02RKa6VigFAKA0NsbREKX+0b5R+vkKpr1lSjfng04XDutO3HJA8RiUkiZi1+OYnx5+teLK97s+ihBxdlsS/dDYMWFw6BIo0kZE7VlQKzkAnvEC7EFjx8TXvQTUUmfM1pKVSTW1zuVIqK69tO1+zwseHB7073b9iIhj8XKDqM1W0lrcprStG3eV5ups4G8smiAFm8ci8h1Y2A8xXn4+tmlkXH3/AKPV+HUejpZ+Zfbj9nJ3jSeeZ8UVzowBsmuRRwGXjlA51PDThCOTknXpTvm4ObglMhCxqzseCoCx16LrWuTS1ehlV3sWb7OMBkkaGVSC2GcOrCxGdiSpHTNavIxNROWbi/4NrTjRXoRveFzJ2UravkVJDzJQWueulvhW/ATytw4eqPP+LUE4KovJ+/P7nGhQCVMxspYAnkATa/kOPpXp31PBk3kdt7Exx8eARQyL2jDS2ZgPM/0q15VseNQljpyyyeVd9kcd9uSDSJI4h+BRf+I3Ncub1gYS1qNy83p8tiLbTR2JZiWJ5k3qDPUoqMY5Yqxz4XyuD1qmtHNBo3YeeWomfS3sx2l22z4Re7RXiP8Al+76mMofWsdN3iX145Zs6GO3RwEzl5cHh3cm7MY1ux8SbanzqVkV55Wtcz4Td7CRKVjwsCK3vBYkUHzAGvrXSKdj1s7YOFgYtBhoYmPFo41U+V1HDpXEkicpyl2nc6NdICgFAKAUBWvtA2m3asIyCQAmvS5PzJHpXl4l56vgj6DAQcKKfL1OFuNIgiUz+402YkgkAAjSwvfUW9ag8vSq+yLmqjpScN3f6lyQTK6q6kFWAKkcCCLg/CvWTTV0fNSi4tp7nuunCivaRiDitrNECcsSpH093O7effy/uipyqKlScyuNLpqyh8/Lk6y4EZUgAsCFeXoo9xPh3j5ivMwkHUnnfH3PbxNRQjlj5e/sef7AdG7WA5Wvmym+U/DVSfEeoNbq1CFXfcy0sTKCs9UZsTjsSkTLh4Y8Opvmey52Jvc5YgB6lj5CsscA271Je/UnLEU0+rEybFVkx0SX7xwq36ns7/pWGpbI/M2Npwu/epFtpYY/WJ4O1vXUfzXrTRqZWpdxXXpqpTlDvI9MLivevc+P1i9TdjU2HlSMrq52rDJNxPwpUrkbmGSK44aUuSUjRxWGXwoXRmy1vY5tNbGLQZx/PGPzKf8Ajryl1KsoeqPcqPpaMavo/fvctOrjIKAUAoBQCgFAKAiuxNkriI5ziIjaWS9mDI1lubjgw7zsOXwrJQpZlJzW7PTxdfo5QVKXZXHvyNzZm6eHh0GZ1DZlVyCFPoASPMnjVkcNBSzGeeNqyjlO9V5kFAURuyqSz43GOwZO1kZSftBpCwA6G6jyHSsfxCb6tNG3AQspVOW7LyW/vwJbsTDs6l21eRrk+fGtVKCpwUSutPPLTYkXZC1qkUs5+3ogID/vlXU9Ti3OG+ICbZjvoFSNPjGR+teLKNqb8/yetFOVC3f+zl7faGLE4hZ27NSGKtYnvK11Wyg8Qza12kpSisqux0ijFSexDcfkDNkYMp1BAI4i5FiAdDpXv4bN0aU1ZnzGNyOtJwd09f2dvB4NSGAlCSKzKVa4HH7w04HnamHadNO5m+JVJQxEk4XjptrwuP0YsXgHT31IB4Hip8iNDVruiinXhU7L/fyNR1I0F7Hj1rqZZZXuaU6aULYs3Nz9otBiBbjcMvUqeHqLr6msGMja1RcHt/DpqSlSfOqPovC4hZEV1N1dQynoRcV1O6uitpp2ZlrpwUAoBQCgFAKAUAoBQHJ3txTRYHFyKbMmHmZfNYmI+Yrq3BSOxWywQQL9sh28gTb5sfhWSMelxLk+D1Euiw0Y82++rLV2NCAo8FFh5nia1S3PPZsTCxoiJixWD7ZCl7A86XsFoyH4/dueKdsSrK0gIYl9QbeI4/AiqXh6c1l2NKxUoxsj83m//q2c2IkCnEgsGKiyjsyRlAv1B1JNYaUujxKj4l1al/iko7WuvuV7ALsvgSPzr3asssG/A+dowzVYx8UdTZ8wd5GN8pcnTjrpz8qrw6y04pnPiLvXk4m/h8W8fusQDxHFT5qdDVydjz50oVO0v38zIzwye+hjb70eq+qH9DXbpkFGtT7LzLue/wA/2jUxOyHsWS0ijmmtvNfeHqKWLI4qF7T6r7n+HsyPTqUYMOIN6rqRUouL5PUw9XJNSXBd3sw22JYTETqneX9ljqP3Wv6MtefQbV4PdHsYuCuqkdmTatBkFAKAUAoBQCgFAKAUBxd9Y82z8Yvjhp//ABNXHsTp9pX7yjtxEMs7k8FAUdP96VGgrRbN+JleVi48DIFS/wB3l1qfJgkRXeHeEqdDdz9kG3/FXRgEdPc/bvbrZtG1BHEgg8PXjUJxsSa0udPah7p8agtytkBOIyLiYG5nOB0kGU/zD8q8/FQy1lJHrYeSnRSfkQjDD+W9/SvUrzTo377HiYWi1irf9bs6e7MalirOEut7kE3seGnn8quUeDyMXOXbSvqSE4GH/HHojV3Ku8xdNW/+f1RiOAj5Tr6q4/Q123id6aot6b+a/YTBOhzRzRZuj5T/ADWpa2zOSrQmstSDt5X+1zW2hhncEyw5v/cjtf1KXU/n1rjuTpThHSnO3hL+7NfbwMW6m1fomIR1JKqdRaxKnRhbxtqNeIFefiY5JqovU+q+H1XXoulLdbe/e5fsUgZQykFWAII4EEXBq0ratoe6HBQCgFAKAUAoBQCgMc6ZlZfEEfEUOrco72abNeJJO0UrIJmRgeKmMBCPRlrkFaNjZVlmbZYG1G7mhsQOIqcdzLJkGTBs0hbQ8avuRue90w6Yk2sA1/kxqM9UXLsFiYiLTjcnnVCKGVvvxgmX69eEfdfqre9/DofQ1GvDNC/casHUyzyvkiLiwbxaw/1+I1qqi3OUYcLUtxUY0oTqLdqxkwj2kQ/iA9G0/WvRZ81JdVkpbD28/wDfz6Vy5iU7mPJ01rp24kjHlS4TZqMhU3UkHobflXLlmjVnqaGOkcm5Ym3jr+dQqQU4uJswc40KinBW8u4tb2Wbb7WAwse9FqvVG5fum48itYqL0yvg93FwWZTjsycVeZBQCgFAKAUAoBQCgFAQnaqjt5ioA7y8PEKlz53rpoXZRr4qTMOvOpIpkcqRMqk8hUyJp7CTVW8b/wDcaSLl2SaGW6WHGqipo0MfgQ0ZUi4IIN+d+NdRC9ncqTH4QwSGF9V/6ZP2lH2Sfvr8xaslSDpyzRPWp1I14ZZepqSKV5/sn/fOttKsqi8Tw8VhXRl3onuFxMcwzRrG+gJVGYOunAqSflerVqfOVITpSyzbXi0mn6r8n7aI8RIp9G//ADS6O/5F3P6fs2MNs6J9O116i356V1WfJVUrVYa5fqYdobAkXVO+OnH4V2UGiVHHQlpLQj2Mw51BBBHI6GoHo05p6o9bpbXOExSSfYvZx4o3vedtGA8VFYq6yTU+GfSYKfT0HSe629/Qv5WBAINwdQRwNWmc/aAUAoBQCgFAKAUAoCCSvd3b70rkeWYkfK1dNL0SRjniubjQ1JMoZxt4ZyqhLanjY1OCIo/Nko2ReA4jrxNcluXLYkuCW2XnfiarZVI3ZVoVkR2lseKbFRJMmeJ5FDLci99BqpBGpHA1LeNicJNao42+fs+nwuaWC8+GGpXjLGOoGrqPvL3vEGxasrpuLvE3QrxqLLURJNy938Nj9mxZwRJGZESZDaRRnZlGb7SgOLBrj11q2lNtXZkxNGKlltoaG2NlYvB3M6jE4cf9UA5lH4vtJ595evKtCnfc8Ov8NUetS08tvkYcNkk1ifX7rWDeh4H5V219jzpOdP8A5F6rb+j3IGHdbMOhrmwWV6qzNTEI1uIYfdbX4eHoRXblkVG+mnivf3I1tSEA5gpX1uPnr8zUKkVOLR62BrypTUr3/Ransu3iE0AgY/WRDu/iTl/DfL5Zay0pf6PdHtYmCv0kdmTirjKKAUAoBQCgFAKA8SkhSRxsbfCgIBg20jDad0W6k21vUjVU3Ok8QVcx4URmZFZvrZCatWiOG3sf3pE5qwI8iBUJFyXVTO9H3cp5A61EqZ0JoriolZwsXDaWNvuSRt6K6k/IVM6ifVAkeIolUZVUKo5AADU34DrQHugIVvFuDHJeTCkQycSn/Sb0HuHqNOnOpKRRUw8ZkQ+m4mBxDMjBuSuLqQOangV6g1apux4uIwMI9Zq3ijqZ4CO+VVuYjNx+oqXU5+h56VdPqarx0MGM2PFIt42YjwsGt6aH5Gjiv9SUMVUhK00vt9diKLnwc6ywuMytce8PMEEDQjQjwJrHXh/vHdH1nwvF9IuhqLR7bb+/qXnsLaqYqCOZODjUc1I0KnqCCK7GWZXLqkHCTizfqRAUAoBQCgFAKAGgIgdmtHIUt3R7nVeXy0rtzTfMrmvt26gIh48VqUe8okaUGECJfgevGu3uQZp4GN1lSS1g5IJPD3jaj7i+HZsSOaJtR3fgagiqRtYV2y2sGt1sa4RZy9p3OhWwOh1vxqSOEs2TMXhQk3NrE+OUkX9bXqJI26AUAoDV2js+KdDHMiuh5H8weIPUa0ONJ6MrfeDcOaC8mEJlj4mM/wB4v7J4OOnvftGpKXeZqmFUtiP4LHnqrA2PEEEcjzBqR5lWhZ2aPW1Zu0WzqGPJuDD15+t663fchRpunK9N2+39eh1PZXt7spzhmP1cx7vSRR/9lFvNR41jj1JuPB9XJ9PRVXnn373Lcq8yCgFAKAUAoBQCgPEkYbjQ6pNbHGx2zZQQYFia/Eyuykeiob/EV1W5O3T3IhvxhMfHEZmysq8RAGAQfebQuR+zw4m1HNRWiuWUoRk7N2IPuZvrIJDBMBIjm6Dn1CknjzsePQ6mSu9XuWNR2Rb+zsZDMgtICeujDoynUGoNNFMonnFyRwd5pUUdSPy4murUrscqLEHGvlw98o9+Qg5Vvz14m3Bf0qVsu4y23Jvh4Qiqi8FAA9P1qAMlAKAUAoBQEZ3s3Qjxf1ins8QBo4GjW+y45jrxHloep2ITpxmrMq3GwyQOYcQhRx8GHJlPBlPj6GxBAkncwVcPKDvwcdZ2imWRfeVldf2kIYflVGIW0lwet8MndSpvn2z6KweJWWNJEN1dVZT4hgCPkamnfUg1Z2ZmrpwUAoBQCgFAKAUAoBQFPe1r2eKFbH4NQhS7zxjQEDUyp91ha7DgRcjUHN3ixbTqWaIhgNtt2QaVGkAsM99R4X4G3K9+PmKojUqqWVNX8TfONPKpWdn3GZ9sqELxxAficX4cSMzNe3lSVasnldr+BCNGk1fWxde4YP8AZ+FZveeJJG0A1kGbgP2qu15MEmm3Y79CIoBQCgFAKAUBzdu7DhxcfZzLe2qsNHQ/eU8j8jwII0oOLMo3evY0uEm7KUdY3A7sij7Q8CLgMvEHxBBPJdaLRGlDoakZra/y8y1PZVtAy7PQHUxO8foLMo9FcD0qFJ3iasVHLUZMKsM4oBQCgFAKAUAoBQCgPEsYZSrC6kEEHgQRYigPl7CSthZpYT3ljkkjYHUHs3KG4Ohvl1FV1qWfrLc9ChWyq0tmbDCTHTw4aPuiRlRQAAFXm1vBVBa3Q0o0cnWluRr1k1aOx9MYeEIqoosqgADoBYVYYTJQCgFAKAUAoBQCgOXvFsKHGQmGYacVYe8jDg6nkRfyIJBuCRXGrkoyys4Ps02LNg48Rh5hqJ8yuPddWjQBh4e5YjkR6mFOLjdF2IqKo013EyqwzigFAKAUAoBQCgFAKAGgPkqXFmSWSQm5eR3J6uxY/M12m7xTNlSKjJpFiew/Zwlxs05t9RGAo/FMWGYeSow/frrZnnsXjUSsUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKAUAoDW2liRHDJIeCI7H91Sf0rjdlc7FXaR8oJhDlUqCbgAgakNbwHjxHrXKc0uq+DdUg31lyWp7CcEwmxD62EahtdLs+nnoh+fjS95XM03aOXm/4RclSKRQCgFAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgOJvtBnwGJXOEXsyXY8Ai6vwB4oCPWozV4tFlJ2mmUVs/d5pAJEkyqVtIOZtpoeWmlQajKzZCrj5Ydyp+LsWj7II1EM9v8AFA9AgA/1qad2yMYtU4t86/Un9SOigFAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAQf2syucLHClwJpQH6qqlsvqwX0BHOqa88sTXg4KVS74IlFu9KEP96qkDMqlcpsONyt1vbX1tasMcW4rKrF1fAUatTPLclXsugZRiTYBCyAAcAVU3HoCta8M24tlWMSTSXcTutJjFAKAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKAj2/GGDYftLawurj5qfSzX9Kz4mOamzThJ5anmaOG2z9JhKQj6wgKL8Bfn5V48aMnNRNUoKEs7ehI9kbPWCJYl1CjU82J1LHqTXvQioxsjz6k3OTkzcqRAUAoBQCgFAKAUAoBQCgFAKAUAoBQCgFAKAUAoDBj8MJYnjPB1Zf4gR+tckrpolGWWSZyd19gphlJGpPPp/re/pbrfPQo5OtLcur1nPQ7taTOKAUAoBQCgFAKAUAoBQCgFAKAUB//2Q=="



  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public db: AngularFireDatabase,
    public loadCtrl: LoadingController,
    public network: Network
  ) {

  }
  // watch network for a connection
  funcaoNetWork() {
    this.connectSubscription = this.network.onDisconnect().subscribe(() => {
      this.unsubscribeNetwork()
      this.load.dismiss()
    });


  }

  subirImage() {

    if (this.network.type !== 'none') {
      this.load = this.loadCtrl.create({
        content: "Salvando dados por favor aguarde..."
      })
      this.load.present()
      this.funcaoNetWork()

      this.saveImageInstitution()
        .then(() => {
          this.load.setContent('Por favor aguarde salvando dados: 50%')
          this.saveImageDocumentation()
            .then(() => {
              this.load.setContent('Por favor aguarde salvando dados: 75%')
              this.saveIdentity()
                .then(() => {
                  this.load.setContent('Por favor aguarde salvando dados: 100%')
                  this.saveIdentity2()
                    .then(() => {
                      this.unsubscribeNetwork()
                      this.load.dismiss()
                      //cancelar onDisonnect()
                      this.db.database.ref('teste').child(this.uid).onDisconnect().cancel()
                    })
                })
            })
        })

    } else {
      alert('Sem internet, verifique sua conexão')
    }




  }


  saveImageInstitution() {
    //salvando a photo da instituição)
    console.log('executeando saveImageInstitution')
    this.load.setContent('Por favor aguarde salvando dados: 25%')

    let refData = this.db.database.ref('teste').push()
    return refData.set({
      institution: this.image
    }).then(() => {
      console.log('then instituition image salva')
      this.uid = refData.key;

      this.refDB = this.db.database.ref('teste').child(this.uid)
    })
  }

  saveImageDocumentation(): Promise<any> {
    let self = this
    return new Promise((resolve, reject) => {
      console.log('getUserInDatabase entry')
      this.refDB.on('value', function (snapshot) {
        //verifica se existe um campo institution nesse nó
        if (snapshot.child("institution").exists()) {
          resolve("Success!")
          self.refDB.update({
            documentation: self.image
          })
        } else {
          //dados não gravados
          reject("Error!")
        }
      })
    })
  }

  saveIdentity(): Promise<any> {
    let self = this
    return new Promise((resolve, reject) => {
      this.refDB.on('value', function (snapshot) {

        if (snapshot.child("institution").exists()) {
          resolve("Success!")
          self.refDB.update({
            identity: self.image
          })
        } else {
          //dados não gravados
          reject("Error!")
        }
      })
    })
  }

  saveIdentity2(): Promise<any> {
    let self = this
    return new Promise((resolve, reject) => {
      console.log('getUserInDatabase entry')
      this.refDB.on('value', function (snapshot) {

        if (snapshot.child("institution").exists()) {
          resolve("Success!")
          self.refDB.update({
            identity2: self.image
          })
        } else {
          //dados não gravados
          reject("Error!")
        }
      })
    })
  }

  unsubscribeNetwork() {
    //depois de salvar as imagens, não preciso mais verificar conexão
    this.connectSubscription.unsubscribe();
  }
}

